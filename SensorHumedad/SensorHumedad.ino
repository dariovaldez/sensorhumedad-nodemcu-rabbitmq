#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

const char* ssid = "j"; //SSID DE RED
const char* pass = "12345678"; //PASSWORD DE LA RED
const char* rabbitmqServer = "192.168.43.239";  //IP DE SERVIDOR MQTT-RABBITMQ
const int mqttPort = 1883; //PUERTO MQTT
const char* mqttUser = "mqtt-sensor";  //USUARIO DE RABBITMQ
const char* mqttPassword = "mqtt-sensor"; //PASSWORD DE USUARIO RABBITMQ

const char* humedad = "";

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  Serial.print("conectando a: ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid,pass);

  //
  while(WiFi.status()!=WL_CONNECTED){
    delay(100);
    Serial.print(".");
  }
  Serial.println("CONECTADO A RED WIFI");
  Serial.println("");
  Serial.println("");
  
  //Conectarse con MQTT
  client.setServer(rabbitmqServer, mqttPort);
  while (!client.connected()) {
    Serial.println("Conectando con MQTT...");
    if (client.connect("Esp8266Client",mqttUser, mqttPassword)) {
      Serial.println("connected");  
    } else {
      Serial.print("failed with state ");
      Serial.println(client.state());
      delay(2000);
    }
  }
  
}


void loop() {
  float valor = analogRead(A0);
  float valorPorcentual = map(valor, 1024, 300, 0, 100);
  Serial.print("Valor del sensor: ");
  Serial.println(valor);
  Serial.print("Valor mapeado:");
  Serial.println(valorPorcentual);

  //Publicar mensaje
  client.publish("sensorHumedad", String(valorPorcentual).c_str());
  Serial.println("Valor enviado al broker");
  Serial.println("");
  Serial.println("");
  delay(500);
  
}
