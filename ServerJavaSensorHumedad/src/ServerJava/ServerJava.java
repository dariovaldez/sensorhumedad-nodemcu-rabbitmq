package ServerJava;

import java.net.*;
import java.io.*;

public class ServerJava {
	
	private static Socket clienteSocket;
    private static ServerSocket serverSocket;
    private static BufferedReader br;
    private static InputStreamReader isr;
    
    private static String humedad = "";
    private static int puerto = 5000;
	
	public static void main(String[] args) {
		
		System.out.println("Server iniciado ....");
        try {
            while (true) {
            	System.out.println("Esperando respuestas del sensor ....");
        
                serverSocket = new ServerSocket(puerto);
                clienteSocket = serverSocket.accept();
                
                isr = new InputStreamReader(clienteSocket.getInputStream());
                br = new BufferedReader(isr);
                humedad = br.readLine();
                System.out.println("Valor recibido del sensor: " + humedad + "\n");
                
                
                isr.close();
                br.close();
                clienteSocket.close();
                serverSocket.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
		
	}
}
