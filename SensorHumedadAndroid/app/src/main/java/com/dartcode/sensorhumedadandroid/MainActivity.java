package com.dartcode.sensorhumedadandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    TextView txtValorHumedad;
    String message;
    private Consumer consumer;

    private final static String QUEUE_NAME = "hello";
    private static final String EXCHANGE_NAME = "amq.topic";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtValorHumedad = findViewById(R.id.txtValorHumedad);
        IniciarRabbit iniciarRabbit = new IniciarRabbit();
        iniciarRabbit.execute();
    }

    private class IniciarRabbit extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost("192.168.1.142");
                factory.setUsername("mqtt-sensor");
                factory.setPassword("mqtt-sensor");
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel();
                channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
                String queueName = channel.queueDeclare().getQueue();
                String bindingKey = "#";

                channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);

                consumer = new DefaultConsumer(channel) {
                    @Override
                    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                        message = new String(body, "UTF-8");
                        Log.e("rabbitMQ", " [x] Received message: '" + message + "'");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txtValorHumedad.setText(message + " %");
                            }
                        });
                    }
                };
                channel.basicConsume(queueName, true, consumer);

            }catch (Exception e){
                Log.e("rabbitMQ", e.getMessage());
            }
            return null;
        }
    }

}
