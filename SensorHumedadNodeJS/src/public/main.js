const socket = io();

var g = new JustGage({
    id: "gauge",
    title: "SENSOR DE HUMEDAD",
    titleFontColor: "#09000f",
    min: 0,
    value: 0,
    max: 100,
    labelFontColor: "#09000f",
    levelColors: ["#6cffb6", "#2492ff", "#000093"]
});

socket.on('valorSensor', (data) => {
    g.refresh(parseInt(data));
});