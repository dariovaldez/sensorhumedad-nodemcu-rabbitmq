const http = require('http');
const express = require('express');
const app = express();
const server = http.createServer(app);
const amqp = require('amqplib/callback_api');
const socketIO = require('socket.io');
const io = socketIO.listen(server);
const mariadb = require('mariadb');

app.use(express.static(__dirname + '/public'));

server.listen(3000, () => {
    console.log("Server on port", 3000);
});

//Conección a MariaDB
const conectarMariaDB = mariadb.createPool({
    host: 'localhost',
    user: 'dario',
    password: '1234',
    port: '3306',
    database: 'servidorIOT'
})

//Función de insertar datos en MARIADB
async function insertarDatos(dato){
    let conn;
    try {
        conn = await conectarMariaDB.getConnection();
        const res = await conn.query("INSERT INTO sensorHumedad(datoSensor) values (?)", [dato]);
        console.log(res);
    } catch (error) {
        throw error;
    } finally {
        if (conn) return conn.end();
    }
}

//Conectar con RabbitMQ
amqp.connect('amqp://localhost', (error0, connection) => {
    
    connection.createChannel((error1, channel) => {
        var exchange = 'amq.topic';
        var queue = 'webPage';
        var key = '#';

        channel.assertExchange(exchange, 'topic', {
            durable: true
        });

        channel.assertQueue(queue, {
            exclusive: true
        }, (error2, q) => {
            channel.bindQueue(q.queue, exchange, key);

            channel.consume(q.queue, (mensaje) => {
                let dato = mensaje.content.toString();
                insertarDatos(dato);
                io.emit('valorSensor', mensaje.content.toString());
            }, {
                noAck: true
            });
        });
    });
});