--Crear base de datos
CREATE DATABASE servidorIOT;

--Usar la base de datos
USE servidorIOT;

--Crear las tablas

CREATE TABLE sensorHumedad(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    datoSensor INT(5) NOT NULL,
    fechaHora TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

--Describir la tabla
DESCRIBE sensorHumedad;

SELECT * FROM sensorHumedad;